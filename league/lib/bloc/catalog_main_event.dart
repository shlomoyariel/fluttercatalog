import 'package:flutter/material.dart';

abstract class CatalogMainEvent {}

class SeasonCatalogMainRequest extends CatalogMainEvent {
  final String seasonId;

  SeasonCatalogMainRequest({@required this.seasonId});
}
