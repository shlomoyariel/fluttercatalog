import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:league/bloc/catalog_main_event.dart';
import 'package:league/bloc/catalog_main_state.dart';
import 'package:league/repositories/catalog_main_repository.dart';

class CatalogMainBloc extends Bloc<CatalogMainEvent, CatalogMainState> {
  final _standingsRepository = CatalogMainRepository();

  CatalogMainBloc() : super(CatalogMainInitial());

  @override
  Stream<CatalogMainState> mapEventToState(CatalogMainEvent event) async* {
    if (event is SeasonCatalogMainRequest) {
      yield CatalogMainLoadInProgress();

      try {
        final mainCatalogResponse = await getCatalogMainRepository();
        yield CatalogMainPageLoadSuccess(
          productCategoryList: mainCatalogResponse.productCategoryList,
          productCollectionList: mainCatalogResponse.productCollectionList,
          productThematicList: mainCatalogResponse.productThematicList,
          thematicCollectionList: mainCatalogResponse.thematicCollectionList,
          products: mainCatalogResponse.products,
        );
      } catch (e) {
        yield CatalogMainPageLoadFailed(error: e);
      }
    }
  }

  Future<CatalogMainResponse> getCatalogMainRepository() async {
    CatalogMainResponse data = _standingsRepository.data;
    if (data == null) {
      await _standingsRepository.getCatalogMain();
      data = _standingsRepository.data;
    }
    return data;
  }
}
