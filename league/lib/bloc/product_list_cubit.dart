import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:league/models/product_list.dart';
import 'package:league/repositories/catalog_main_repository.dart';

class ProductListCubit extends Cubit<ProductList> {
  final _catalogRepository = CatalogMainRepository();

  ProductListCubit() : super(null);
  void setProductCategory(String categoryId, String categoryName) async {
    final products = await _catalogRepository.getProductCategory(categoryId);
    emit(ProductList(products: products.products, categoryName: categoryName));
  }

  void setProductCollection(String collectionId, String collectionName) async {
    final products =
        await _catalogRepository.getProductCollection(collectionId);
    emit(
        ProductList(products: products.products, categoryName: collectionName));
  }
}
