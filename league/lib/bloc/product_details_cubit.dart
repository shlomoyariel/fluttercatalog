import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:league/models/product_details.dart';
import 'package:league/repositories/catalog_main_repository.dart';

class ProductDetailsCubit extends Cubit<ProductDetails> {
  final _catalogRepository = CatalogMainRepository();

  ProductDetailsCubit() : super(null);
  void setProductDetails(String productId) async {
    emit(ProductDetails(null));
    final product = await _catalogRepository.getProductDetails(productId);
    emit(ProductDetails(product));
  }
}
