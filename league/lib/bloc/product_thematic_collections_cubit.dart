import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:league/models/product_collection.dart';
import 'package:league/repositories/catalog_main_repository.dart';

class ProductThematicCollectoinsCubit
    extends Cubit<ProductThematicCollections> {
  final _catalogRepository = CatalogMainRepository();

  ProductThematicCollectoinsCubit() : super(null);
  void setThematicCollections(String thematicId, String thematicName) async {
    emit(null);
    final productThematicCollections =
        await _catalogRepository.getProductCollectionByThematic(thematicId);
    emit(ProductThematicCollections(
        collectionList: productThematicCollections,
        thematicName: thematicName));
  }
}
