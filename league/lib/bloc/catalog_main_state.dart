import 'package:flutter/material.dart';
import 'package:league/models/product_category.dart';
import 'package:league/models/product_collection.dart';
import 'package:league/models/product_details.dart';
import 'package:league/models/product_thematic.dart';
import 'package:league/models/product_thematic_collection.dart';

abstract class CatalogMainState {}

class CatalogMainInitial extends CatalogMainState {}

class CatalogMainLoadInProgress extends CatalogMainState {}

class CatalogMainPageLoadSuccess extends CatalogMainState {
  final List<ProductThematic> productThematicList;
  final List<ProductCollection> productCollectionList;
  final List<ProductCategory> productCategoryList;
  final List<ThematicCollection> thematicCollectionList;
  final List<Product> products;

  CatalogMainPageLoadSuccess(
      {this.productThematicList,
      this.productCollectionList,
      this.productCategoryList,
      this.thematicCollectionList,
      this.products});
}

class CatalogMainPageLoadFailed extends CatalogMainState {
  final Error error;

  CatalogMainPageLoadFailed({@required this.error});
}
