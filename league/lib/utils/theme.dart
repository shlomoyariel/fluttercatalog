import 'package:flutter/material.dart';

ThemeData basicTheme() {
  TextTheme _basicTextTheme(TextTheme base) {
    return base.copyWith(
        headline1: base.headline1.copyWith(
          fontFamily: 'ABChanelCorpo-SemiBold',
          fontSize: 26.0,
          color: Colors.black,
        ),
        headline2: base.headline2.copyWith(
          fontFamily: 'ABChanelCorpo-SemiBold',
          fontSize: 16.0,
          color: Colors.grey[700],
        ),
        headline3: base.headline3.copyWith(
          fontFamily: 'HelveticaNeue-Bold',
          fontSize: 15.0,
          color: Colors.black,
        ),
        headline4: base.headline4.copyWith(
          fontFamily: 'ABChanelCorpo-Light',
          fontSize: 13.0,
          color: Colors.black,
        ),
        headline5: base.headline5.copyWith(
          fontFamily: 'ABChanelCorpo-Light',
          fontSize: 13.0,
          color: Colors.grey[600],
        ),
        headline6: base.headline3.copyWith(
          fontFamily: 'HelveticaNeue-Bold',
          fontSize: 18.0,
          color: Colors.black,
        ),
        subtitle1: base.subtitle1.copyWith(
          fontFamily: 'HelveticaNeue-Bold',
          fontSize: 18.0,
          color: Colors.grey,
        ),
        bodyText2: base.bodyText2.copyWith(color: Colors.black));
  }

  final ThemeData base = ThemeData.light();
  return base.copyWith(
      appBarTheme: AppBarTheme(
          iconTheme: IconThemeData(color: Colors.black),
          color: Colors.grey[200],
          brightness: Brightness.light,
          textTheme: _basicTextTheme(base.textTheme)),
      textTheme: _basicTextTheme(base.textTheme),
      //textTheme: Typography().white,
      primaryColor: Colors.grey[200],
      //primaryColor: Color(0xff4829b2),
      indicatorColor: Colors.black,
      scaffoldBackgroundColor: Colors.black,
      accentColor: Colors.black,
      iconTheme: IconThemeData(
        color: Colors.black,
        size: 20.0,
      ),
      buttonColor: Colors.black,
      backgroundColor: Colors.black,
      tabBarTheme: base.tabBarTheme.copyWith(
        labelColor: Color(0xffce107c),
        unselectedLabelColor: Colors.grey,
      ));
}
