import 'dart:convert';
import 'dart:developer';
import 'package:http/http.dart' as http;
import 'package:league/models/product_category.dart';
import 'package:league/models/product_collection.dart';
import 'package:league/models/product_details.dart';
import 'package:league/models/product_list.dart';
import 'package:league/models/product_thematic.dart';
import 'package:league/models/product_thematic_collection.dart';

class CatalogMainRepository {
  final baseUrl = '2cc2f67f-e3ad-4729-ba68-88fbfe6f4cae.mock.pstmn.io';
  final client = http.Client();
  CatalogMainResponse _data;
  CatalogMainResponse get data => _data;

  Future<CatalogMainResponse> getCatalogMain() async {
    final uri = Uri.http(baseUrl, '/productsMainPage');
    final response = await client.get(uri);
    final json = jsonDecode(response.body);
    _data = CatalogMainResponse.fromJson(json);
    return CatalogMainResponse.fromJson(json);
  }

  Future<ProductList> getProductCategory(String categoryId) async {
    // log('getProductCategory: $categoryId');
    // final uri = Uri.http(baseUrl, '/productsMainPage');
    // final response = await client.get(uri);
    // final json = jsonDecode(response.body);
    // return ProductList.fromJson(json);
    if (_data == null || _data.products == null) {
      await this.getCatalogMain();
    }
    List<Product> products =
        _data.products.where((f) => f.categoryId == categoryId).toList();
    String categoryName = products.length > 0 ? products[0].categoryName : '';
    ProductList productList =
        new ProductList(products: products, categoryName: categoryName);
    return productList;
  }

  Future<ProductList> getProductCollection(String collectionId) async {
    // log('getProductCategory: $categoryId');
    // final uri = Uri.http(baseUrl, '/productsMainPage');
    // final response = await client.get(uri);
    // final json = jsonDecode(response.body);
    // return ProductList.fromJson(json);
    if (_data == null || _data.products == null) {
      await this.getCatalogMain();
    }
    List<Product> products =
        _data.products.where((f) => f.collectionId == collectionId).toList();
    String categoryName = products.length > 0 ? products[0].categoryName : '';
    ProductList productList =
        new ProductList(products: products, categoryName: categoryName);
    return productList;
  }

  Future<Product> getProductDetails(String productId) async {
    // log('getProductCategory: $productId');
    // final uri = Uri.http(baseUrl, '/productDetails');
    // final response = await client.get(uri);
    // final json = jsonDecode(response.body);
    // return Product.fromJson(json);
    //final uri = Uri.http(baseUrl, '/productsMainPage');
    //final response = await client.get(uri);
    //final json = jsonDecode(response.body);
    //_data = CatalogMainResponse.fromJson(json);
    if (_data == null || _data.products == null) {
      await this.getCatalogMain();
    }
    Product product =
        _data.products.firstWhere((product) => product.id == productId);
    return product;
  }

  Future<List<ProductCollection>> getProductCollectionByThematic(
      String thematicId) async {
    if (_data == null || _data.productCollectionList == null) {
      await this.getCatalogMain();
    }
    final thematicCollectionIds = _data.thematicCollectionList
        .where((f) => f.thematicId == thematicId)
        .map((e) => e.collectionId)
        .toList();
    List<ProductCollection> collections = _data.productCollectionList
        .where((f) => thematicCollectionIds.contains(f.id))
        .toList();
    return collections;
  }
}

class CatalogMainResponse {
  CatalogMainResponse(
      {this.productThematicList,
      this.productCollectionList,
      this.productCategoryList,
      this.thematicCollectionList,
      this.products});

  List<ProductThematic> productThematicList;
  List<ProductCollection> productCollectionList;
  List<ProductCategory> productCategoryList;
  List<ThematicCollection> thematicCollectionList;
  List<Product> products;

  factory CatalogMainResponse.fromJson(Map<String, dynamic> json) {
    final thematicCollectionList = (json['ThematicCollection'] as List)
        .map((listingJson) => ThematicCollection.fromJson(listingJson))
        .toList();
    final productThematicList = (json['productThematic'] as List)
        .map((listingJson) => ProductThematic.fromJson(listingJson))
        .toList();
    final productCollectionList = (json['productCollections'] as List)
        .map((listingJson) => ProductCollection.fromJson(listingJson))
        .toList();
    final productCategoryList = (json['productCategories'] as List)
        .map((listingJson) => ProductCategory.fromJson(listingJson))
        .toList()
        .where((f) => f.numProducts > 0)
        .toList();
    productThematicList.sort((a, b) => a.name.compareTo(b.name));
    productCollectionList.sort((a, b) => a.name.compareTo(b.name));
    productCategoryList.sort((a, b) => a.name.compareTo(b.name));
    final products = (json['products'] as List)
        .map((listingJson) => Product.fromJson(listingJson))
        .toList();

    return CatalogMainResponse(
        productCategoryList: productCategoryList,
        productCollectionList: productCollectionList,
        productThematicList: productThematicList,
        thematicCollectionList: thematicCollectionList,
        products: products);
  }
}
