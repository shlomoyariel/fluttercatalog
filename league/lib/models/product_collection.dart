class ProductThematicCollections {
  String thematicName;
  List<ProductCollection> collectionList;
  ProductThematicCollections({this.collectionList, this.thematicName});
}

class ProductCollection {
  ProductCollection({this.id, this.name, this.imageUrl, this.numProducts});

  String id;
  String name;
  String imageUrl;
  int numProducts;

  ProductCollection.fromJson(Map<String, dynamic> json) {
    this.id = json['id'];
    this.name = json['name'];
    this.imageUrl = json['imageUrl'];
    this.numProducts = json['numProducts'];
  }
}
