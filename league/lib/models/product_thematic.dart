class ProductThematic {
  ProductThematic({this.id, this.name, this.imageUrl});

  String id;
  String name;
  String imageUrl;

  ProductThematic.fromJson(Map<String, dynamic> json) {
    this.id = json['id'];
    this.name = json['name'];
    this.imageUrl = json['imageUrl'];
  }
}
