class ProductDetails {
  Product product;
  ProductDetails(Product product) {
    this.product = product;
  }
}

class Product {
  String name;
  String id;
  String categoryId;
  String imageThumbnail;
  double price;
  String desc;
  String sku;
  String categoryName;
  String metals;
  String collectionId;
  bool isSuggested;

  Product.fromJson(Map<String, dynamic> json) {
    this.id = json['id'];
    this.name = json['name'];
    this.categoryId = json['categoryId'];
    this.imageThumbnail = json['imageThumbnail'];
    this.price = json['price'];
    this.sku = json['sku'];
    this.desc = json['desc'];
    this.categoryName = json['categoryName'];
    this.metals = json['metals'];
    this.collectionId = json['collectionId'];
    this.isSuggested = json['isSuggested'];
  }
}
