class ThematicCollection {
  ThematicCollection(
      {this.id,
      this.thematicName,
      this.collectionName,
      this.collectionId,
      this.thematicId});

  String id;
  String thematicName;
  String collectionName;
  String collectionId;
  String thematicId;

  ThematicCollection.fromJson(Map<String, dynamic> json) {
    this.id = json['id'];
    this.thematicName = json['thematicName'];
    this.collectionName = json['collectionName'];
    this.collectionId = json['collectionId'];
    this.thematicId = json['thematicId'];
  }
}
