class ProductCategory {
  ProductCategory({this.id, this.name, this.imageUrl, this.numProducts});

  String id;
  String name;
  String imageUrl;
  int numProducts;

  ProductCategory.fromJson(Map<String, dynamic> json) {
    this.id = json['id'];
    this.name = json['name'];
    this.imageUrl = json['imageUrl'];
    this.numProducts = json['numProducts'];
  }
}
