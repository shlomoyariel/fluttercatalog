import 'package:league/models/product_details.dart';

class ProductList {
  final List<Product> products;
  final String categoryName;
  ProductList({this.products, this.categoryName});

  factory ProductList.fromJson(Map<String, dynamic> json) {
    final products = (json['products'] as List)
        .map((listingJson) => Product.fromJson(listingJson))
        .toList();
    String categoryName =
        products.length > 0 ? products[0].categoryName : 'Products';
    return ProductList(products: products, categoryName: categoryName);
  }
}
