import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:league/bloc/product_details_cubit.dart';
import 'package:league/models/product_details.dart';

class ProductDetailsView extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<ProductDetailsCubit, ProductDetails>(
        builder: (context, state) {
      if (state == null || state.product == null) {
        return Center(
          child: CircularProgressIndicator(),
        );
      } else {
        return Container(
            color: Colors.grey[200],
            child: CustomScrollView(slivers: [
              SliverAppBar(
                floating: true,
                expandedHeight: 50,
                flexibleSpace: FlexibleSpaceBar(
                  title: Text(
                    "",
                    style: Theme.of(context).textTheme.headline6,
                  ),
                ),
              ),
              SliverPersistentHeader(
                pinned: true,
                floating: false,
                delegate: Delegate(Colors.white, state.product.name,
                    state.product.imageThumbnail),
              ),
              SliverPadding(
                padding: const EdgeInsets.symmetric(horizontal: 16.0),
                sliver: SliverList(
                  delegate: SliverChildListDelegate([
                    Text(state.product.name,
                        style: Theme.of(context).textTheme.headline6),
                    Text(state.product.sku,
                        style: Theme.of(context).textTheme.headline6),
                    Text(state.product.desc,
                        style: Theme.of(context).textTheme.headline6),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        _buildButtonColumn(Colors.red, Icons.call, 'CALL'),
                        _buildButtonColumn(
                            Colors.green, Icons.near_me, 'ROUTE'),
                        _buildButtonColumn(Colors.blue, Icons.share, 'SHARE'),
                      ],
                    ),
                  ]),
                ),
              )
            ]));
      }
    });
  }

  Column _buildButtonColumn(Color color, IconData icon, String label) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Icon(icon, color: color),
        Container(
          margin: const EdgeInsets.only(top: 8),
          child: Text(
            label,
            style: TextStyle(
              fontSize: 12,
              fontWeight: FontWeight.w400,
              color: color,
            ),
          ),
        ),
      ],
    );
  }
}

class Delegate extends SliverPersistentHeaderDelegate {
  final Color backgroundColor;
  final String _title;
  final String imageURL;

  Delegate(this.backgroundColor, this._title, this.imageURL);

  @override
  Widget build(
      BuildContext context, double shrinkOffset, bool overlapsContent) {
    return Container(
        color: backgroundColor,
        child: Center(
          child: Image.network(imageURL,
              width: 600, height: 400, fit: BoxFit.fitWidth),
        ));
  }

  @override
  double get maxExtent => 400;

  @override
  double get minExtent => 50;

  @override
  bool shouldRebuild(SliverPersistentHeaderDelegate oldDelegate) {
    return true;
  }
}
