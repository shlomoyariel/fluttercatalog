import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:league/bloc/product_thematic_collections_cubit.dart';
import 'package:league/models/product_thematic.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class ProductThematicsView extends StatelessWidget {
  // 2
  final List<ProductThematic> thematicList;

  const ProductThematicsView({Key key, this.thematicList}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 410.0,
      child: ListView.separated(
        separatorBuilder: (BuildContext context, int index) {
          return SizedBox(width: 30);
        },
        shrinkWrap: true,
        padding: EdgeInsets.only(left: 20.0, right: 20, bottom: 20),
        itemCount: thematicList.length,
        scrollDirection: Axis.horizontal,
        itemBuilder: (context, index) {
          final standingsLine = thematicList[index];
          return GestureDetector(
            onTap: () {
              // Set current selected category in the state
              BlocProvider.of<ProductThematicCollectoinsCubit>(context)
                  .setThematicCollections(standingsLine.id, standingsLine.name);
              // Navigate to the category list using router
              Navigator.of(context).pushNamed('/collectionList');
            },
            child: Card(
              elevation: 3,
              shadowColor: Colors.black12,
              color: Colors.white,
              child: ClipRRect(
                borderRadius: BorderRadius.circular(5.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    Container(
                      width: MediaQuery.of(context).size.width - 80,
                      height: 320,
                      decoration: BoxDecoration(
                        image: DecorationImage(
                          fit: BoxFit.fitHeight,
                          image: NetworkImage(standingsLine.imageUrl),
                        ),
                      ),
                    ),
                    Expanded(
                      child: Container(
                          padding: EdgeInsets.only(left: 20.0, top: 20),
                          child: Text(standingsLine.name,
                              style: Theme.of(context).textTheme.headline3)),
                    )
                  ],
                ),
              ),
            ),
          );
        },
      ),
    );
  }
}
