import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:league/bloc/product_list_cubit.dart';
import 'package:league/models/product_category.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class ProductCategoryListItemView extends StatelessWidget {
  // 2
  final ProductCategory category;

  const ProductCategoryListItemView({Key key, this.category}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    // 3
    return GestureDetector(
      onTap: () {
        // Set current selected category in the state
        BlocProvider.of<ProductListCubit>(context)
            .setProductCategory(category.id, category.name);
        // Navigate to the category list using router
        Navigator.of(context).pushNamed(
          '/productList',
          // arguments: ScreenArguments(
          //   category.name,
          // ),
        );
      },
      child: Card(
        child: ListTile(
          title: Text(
            category.name,
            style: Theme.of(context).textTheme.headline6,
          ),
          subtitle: Text(category.numProducts.toString()),
          leading: ConstrainedBox(
            constraints: BoxConstraints(
              minWidth: 44,
              minHeight: 44,
              maxWidth: 44,
              maxHeight: 44,
            ),
            child: Image.network(category.imageUrl, fit: BoxFit.cover),
          ),
        ),
      ),
    );
  }
}
