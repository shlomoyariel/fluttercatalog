import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:league/app_router.dart';
import 'package:league/bloc/catalog_main_bloc.dart';
import 'package:league/bloc/product_list_cubit.dart';
import 'package:league/utils/theme.dart';

import 'bloc/catalog_main_event.dart';
import 'bloc/product_details_cubit.dart';
import 'bloc/product_thematic_collections_cubit.dart';

void main() {
  runApp(MyApp(
    appRouter: AppRouter(),
  ));
}

class MyApp extends StatelessWidget {
  final AppRouter appRouter;

  const MyApp({Key key, @required this.appRouter}) : super(key: key);

  @override
  Widget build(BuildContext myAppContext) {
    return MultiBlocProvider(
      providers: [
        BlocProvider<CatalogMainBloc>(
          create: (counterCubitContext) => CatalogMainBloc()
            ..add(SeasonCatalogMainRequest(seasonId: '2014')),
        ),
        BlocProvider<ProductListCubit>(
          create: (counterCubitContext) => ProductListCubit(),
        ),
        BlocProvider<ProductDetailsCubit>(
          create: (counterCubitContext) => ProductDetailsCubit(),
        ),
        BlocProvider<ProductThematicCollectoinsCubit>(
          create: (counterCubitContext) => ProductThematicCollectoinsCubit(),
        ),
      ],
      child: MaterialApp(
        title: 'Flutter Demo',
        theme: basicTheme(),
        // theme: ThemeData(
        //   primarySwatch: Colors.blue,
        //   visualDensity: VisualDensity.adaptivePlatformDensity,
        // ),
        onGenerateRoute: appRouter.onGenerateRoute,
      ),
    );
  }
}

// void main() {
//   runApp(MyApp(
//     appRouter: AppRouter(),
//   ));
// }

// class MyApp extends StatefulWidget {
//   final AppRouter appRouter;
//   const MyApp({Key key, @required this.appRouter}) : super(key: key);
//   @override
//   State<StatefulWidget> createState() => _MyAppState();
// }

// // class _MyAppState extends State<MyApp> {
// //   @override
// //   Widget build(BuildContext context) {
// //     return MaterialApp(
// //         title: 'Flutter Demo',
// //         theme: ThemeData(
// //           primarySwatch: Colors.blue,
// //         ),
// //         home: screen1());
// //   }
// // }

// class _MyAppState extends State<MyApp> {
//   @override
//   Widget build(BuildContext context) {
//     return MaterialApp(
//         title: 'Flutter Demo',
//         theme: basicTheme(),
//         darkTheme: AppTheme.darkTheme,
//         home: MyBottomNavigationBar());
//   }
// }

// screen1() {
//   final teamDetailsCubit = TeamDetailsCubit();
//   return MultiBlocProvider(providers: [
//     BlocProvider(
//         create: (context) =>
//             CatalogMainBloc()..add(SeasonCatalogMainRequest(seasonId: '2014'))),
//     BlocProvider(
//         create: (context) => NavCubit(teamDetailsCubit: teamDetailsCubit)),
//     BlocProvider(create: (context) => teamDetailsCubit)
//   ], child: AppNavigator());
// }

// screen2() {
//   return Center(
//     child: Text('Screen 2'),
//   );
// }

// screen3() {
//   return Center(
//     child: Text('Screen 3'),
//   );
// }

// class MyBottomNavigationBar extends StatefulWidget {
//   @override
//   _MyBottomNavigationBarState createState() => _MyBottomNavigationBarState();
// }

// class _MyBottomNavigationBarState extends State<MyBottomNavigationBar> {
//   int _index = 0;
//   final teamDetailsCubit = TeamDetailsCubit();
//   List<Widget> _children = [screen1(), screen2(), screen3()];
//   tapped(int tappedIndex) {
//     setState(() {
//       teamDetailsCubit.clearTeamPage();
//       _index = tappedIndex;
//     });
//   }

//   @override
//   Widget build(BuildContext context) {
//     return new Scaffold(
//       body: _children[_index],
//       bottomNavigationBar:
//           BottomNavigationBar(onTap: tapped, currentIndex: _index, items: [
//         BottomNavigationBarItem(icon: Icon(Icons.home), label: "Home"),
//         BottomNavigationBarItem(icon: Icon(Icons.settings), label: "Settings"),
//         BottomNavigationBarItem(icon: Icon(Icons.contacts), label: "Contacts"),
//       ]),
//     );
//   }
// }
