import 'package:flutter/material.dart';
import 'package:league/views/catalog%20main%20view/catalog_main_view.dart';
import 'package:league/views/product%20details%20view/product_details_view.dart';
import 'package:league/views/product%20list%20view/product_list_view.dart';
import 'package:league/views/product_thematic_collections_view.dart';

class AppRouter {
  Route onGenerateRoute(RouteSettings settings) {
    switch (settings.name) {
      case '/':
        return MaterialPageRoute(
          builder: (_) => CatalogMainView(),
        );
      case '/productList':
        return MaterialPageRoute(
          builder: (_) => ProductListView(),
        );
      case '/productDetails':
        return MaterialPageRoute(
          builder: (_) => ProductDetailsView(),
        );
      case '/collectionList':
        return MaterialPageRoute(
          builder: (_) => ProductCollectionListView(),
        );
      case '/collectionList/productList':
        return MaterialPageRoute(
          builder: (_) => ProductListView(),
        );
      default:
        return null;
    }
  }
}
